import { connect } from 'react-redux';
import Calendar from '../components/Calendar';
import { makeGoogleRequest, SCOPE_CALENDAR_READONLY } from '../googleHelper';
import { setEndDate, setEvents, setStartDate, setTotalHours, setEventsFilter } from '../redux';
import { calcTotalHours } from '../util';

async function fetchEvents(dispatch, apiKey, clientId, calendarId, startDate, endDate) {
    const timeMin = startDate.toISOString();
    const timeMax = endDate.toISOString();
    const path = `https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events?timeMin=${timeMin}&timeMax=${timeMax}`;
    const { result: { items } } = await makeGoogleRequest(apiKey, clientId, SCOPE_CALENDAR_READONLY, path);
    const events = items
        .filter(item => item.start && item.start.dateTime && item.end && item.end.dateTime) // ensure we don't count 'all day' events
        .sort(({ start: { dateTime: a } }, { start: { dateTime: b } }) => a < b ? -1 : a > b ? 1 : 0); // ensure events are sorted by start time

    dispatch(setTotalHours(calcTotalHours(events)));
    dispatch(setEvents(events));
}

const mapStateToProps = ({ googleApiKey, googleWebClientId, currentCalendar: id, calendars, startDate, endDate, totalHours, events, eventsFilter }) => {
    const filteredEvents = eventsFilter ? events.filter(({ summary }) => summary ? summary.toLowerCase().includes(eventsFilter) : false) : events;
    const filteredTotalHours = totalHours === null ? null : eventsFilter ? calcTotalHours(filteredEvents) : totalHours;

    return {
        googleApiKey,
        googleWebClientId,
        id,
        summary: calendars.find(cal => cal.id === id).summary,
        startDate,
        endDate,
        totalHours: filteredTotalHours,
        events: filteredEvents,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onStartDateChanged: (apiKey, clientId, calendarId, startDate, endDate) => {
            startDate.setHours(0,0,0,0);
            dispatch(setStartDate(startDate));
            fetchEvents(dispatch, apiKey, clientId, calendarId, startDate, endDate);
        },
        onEndDateChanged: (apiKey, clientId, calendarId, startDate, endDate) => {
            endDate.setHours(23,59,59,99);
            dispatch(setEndDate(endDate));
            fetchEvents(dispatch, apiKey, clientId, calendarId, startDate, endDate);
        },
        onFilterChanged: (filter) => {
            dispatch(setEventsFilter(filter));
        },
        fetchEvents: (apiKey, clientId, calendarId, startDate, endDate) => {
            fetchEvents(dispatch, apiKey, clientId, calendarId, startDate, endDate);
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);