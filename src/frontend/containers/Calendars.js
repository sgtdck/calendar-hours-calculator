import { connect } from 'react-redux';
import Calendars from '../components/Calendars';
import { makeGoogleRequest, SCOPE_CALENDAR_READONLY } from '../googleHelper';
import { addCalendars, setCurrentCalendar } from '../redux';

const mapStateToProps = ({ googleApiKey, googleWebClientId, calendars, currentCalendar }) => {
    return {
        googleApiKey,
        googleWebClientId,
        calendars,
        currentCalendar
    };
};

const mapDispatchToProps = (dispatch) => ({
    fetchCalendarList: async (apiKey, clientId) => {
        const path = 'https://www.googleapis.com/calendar/v3/users/me/calendarList';
        const { result: { items } } = await makeGoogleRequest(apiKey, clientId, SCOPE_CALENDAR_READONLY, path);

        dispatch(addCalendars(items));
    },
    onCalendarClick: calendarId => {
        dispatch(setCurrentCalendar(calendarId));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Calendars);