import { connect } from 'react-redux';
import App from '../components/App';

const mapStateToProps = ({ calendars, currentCalendar }) => ({ calendars, currentCalendar });

export default connect(mapStateToProps)(App);