import React, { Component, Fragment } from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import format from 'date-fns/format';
import { PulseLoader } from 'react-spinners';
import { calcEventDurationMinutes } from '../util';

export const DATE_FORMAT = "YYYY-MM-DD";
export const TIME_FORMAT = "HH:mm";

class Calendar extends Component {
    componentDidUpdate(prevProps) {
        const { googleApiKey, googleWebClientId, startDate, endDate, id, fetchEvents } = this.props;

        if (id !== prevProps.id) {
            fetchEvents(googleApiKey, googleWebClientId, id, startDate, endDate);
        }
    }

    componentDidMount() {
        const { googleApiKey, googleWebClientId, startDate, endDate, id, fetchEvents } = this.props;
        fetchEvents(googleApiKey, googleWebClientId, id, startDate, endDate);
    }

    render() {
        const { googleApiKey, googleWebClientId, id, summary, startDate, endDate, totalHours, events, onStartDateChanged, onEndDateChanged, onFilterChanged } = this.props;
        const eventsLoaded = totalHours !== null;

        return (<div className="calendar">
            <h4>{summary}</h4>

            <form>
                <div className="form-row">
                    <div className="form-group col-md-2">
                        <label htmlFor="from">From</label>
                        <DayPickerInput id="from" inputProps={{className: "form-control"}}
                                        value={format(startDate, DATE_FORMAT)}
                                        format={DATE_FORMAT}
                                        onDayChange={date => onStartDateChanged(googleApiKey, googleWebClientId, id, date, endDate)}/>
                    </div>
                    <div className="form-group col-md-2">
                        <label htmlFor="to">To</label>
                        <DayPickerInput id="to" inputProps={{className: "form-control"}}
                                        value={format(endDate, DATE_FORMAT)}
                                        format={DATE_FORMAT}
                                        onDayChange={date => onEndDateChanged(googleApiKey, googleWebClientId, id, startDate, date)}/>
                    </div>
                    <div className="form-group col-md-3">
                        <label htmlFor="filter">Filter</label>
                        <input id="filter" className="form-control" type="search" onChange={e => onFilterChanged(e.target.value)}/>
                    </div>
                </div>
                <div className="form-row">
                </div>
            </form>

            {eventsLoaded ? <table className="table table-hover">
                <thead className="thead-light">
                    <tr>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Event</th>
                        <th className="text-right">Hours</th>
                    </tr>
                </thead>
                <tbody>
                {events.map(event => {
                    return (<tr key={event.id}>
                        <td className="date">{format(event.start.dateTime, DATE_FORMAT)}</td>
                        <td className="time">{format(event.start.dateTime, TIME_FORMAT)} - {format(event.end.dateTime, TIME_FORMAT)}</td>
                        <td className="summary w-50">{event.summary}</td>
                        <td className="text-right hours">{Math.floor(calcEventDurationMinutes(event) / 6) / 10}</td>
                    </tr>);
                })}
                </tbody>
                <tfoot>
                    <tr>
                        <th className="table-primary" colSpan="3">Total</th>
                        <th className="table-primary text-right">{totalHours}</th>
                    </tr>
                </tfoot>
            </table> : <PulseLoader/>}
        </div>);
    }
}

export default Calendar;