import React, { Component } from 'react';
import { PulseLoader } from 'react-spinners';

class Calendars extends Component {
    componentDidMount() {
        const { googleApiKey, googleWebClientId, fetchCalendarList } = this.props;
        fetchCalendarList(googleApiKey, googleWebClientId);
    }

    render() {
        const { calendars, currentCalendar, onCalendarClick } = this.props;

        return calendars.length > 0 ? (<div className="nav nav-pills flex-column calendars">
            {calendars.map(calendar => (<a className={"nav-link" + (currentCalendar === calendar.id ? " active" : "")} key={calendar.id} href="#" onClick={e => {
                e.preventDefault();
                onCalendarClick(calendar.id);
            }}
            >{calendar.summary}</a>))}
        </div>) : <PulseLoader />;
    }
}

export default Calendars;