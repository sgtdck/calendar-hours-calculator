import React, { Component } from 'react';
import Calendar from '../containers/Calendar';
import Calendars from '../containers/Calendars';

class App extends Component {
    render() {
        const { calendars, currentCalendar } = this.props;

        return (<div className="app-container">
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">Calendar Hours Calculator</a>
            </nav>
            <div className="container-fluid mt-3">
                <div className="row">
                    <div className="col-sm-2 left">
                        <Calendars />
                    </div>
                    <div className="col-lg-10 main">
                        {currentCalendar ? <Calendar /> : <div className="jumbotron">
                            <h1 className="display-4">Welcome to the Calendar Hours Calculator!</h1>
                            <p className="lead">{calendars.length > 0 ? "Please click a calendar on the left." : "Please authenticate with Google. You may have to allow a pop-up first."}</p>
                        </div>}
                    </div>
                </div>
            </div>
        </div>);
    }
}

export default App;