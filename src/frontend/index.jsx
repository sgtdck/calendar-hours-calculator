import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from "redux";
import App from './containers/App';
import reducer, { setGoogleApiKey, setGoogleWebClientId } from './redux';

// const { API_KEY, CLIENT_ID } = window;
const store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

store.dispatch(setGoogleApiKey(API_KEY));
store.dispatch(setGoogleWebClientId(CLIENT_ID));

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app'),
);