export const SCOPE_CALENDAR_READONLY = 'https://www.googleapis.com/auth/calendar.readonly';

export function makeGoogleRequest(apiKey, clientId, scope, path) {
    return new Promise((resolve, reject) => {
        const updateSigninStatus = async (isSignedIn) => {
            if (isSignedIn) {
                const response = await gapi.client.request({ path });
                resolve(response);
            } else {
                gapi.auth2.getAuthInstance().signIn();
            }
        };

        const request = async () => {
            try {
                await gapi.client.init({
                    apiKey,
                    clientId,
                    scope,
                });

                gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

                // Handle the initial sign-in state.
                updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            } catch (e) {
                reject(e);
            }
        };
        gapi.load('client:auth2', request);
    });
}