const date = new Date();
const initialState = {
    googleAccessToken: null,
    calendars: [],
    startDate: new Date(date.getFullYear(), date.getMonth() - 1, 1),
    endDate: new Date(date.getFullYear(), date.getMonth(), 0),
    totalHours: null,
    events: [],
    eventsFilter: null
};

const SET_GOOGLE_API_KEY = 'SET_GOOGLE_API_KEY';
const SET_GOOGLE_WEB_CLIENT_ID = 'SET_GOOGLE_WEB_CLIENT_ID';
const ADD_CALENDARS = 'ADD_CALENDARS';
const SET_CURRENT_CALENDAR = 'SET_CURRENT_CALENDAR';
const SET_START_DATE = 'SET_START_DATE';
const SET_END_DATE = 'SET_END_DATE';
const SET_TOTAL_HOURS = 'SET_TOTAL_HOURS';
const SET_EVENTS = 'SET_EVENTS';
const SET_EVENTS_FILTER = 'SET_EVENTS_FILTER';

export function setGoogleApiKey(apiKey) {
    return {
        type: SET_GOOGLE_API_KEY,
        apiKey,
    };
}

export function setGoogleWebClientId(clientId) {
    return {
        type: SET_GOOGLE_WEB_CLIENT_ID,
        clientId,
    };
}

export function addCalendars(calendars) {
    return {
        type: ADD_CALENDARS,
        calendars,
    };
}

export function setCurrentCalendar(id) {
    return {
        type: SET_CURRENT_CALENDAR,
        id,
    };
}

export function setStartDate(date) {
    return {
        type: SET_START_DATE,
        date,
    };
}

export function setEndDate(date) {
    return {
        type: SET_END_DATE,
        date,
    };
}

export function setTotalHours(hours) {
    return {
        type: SET_TOTAL_HOURS,
        hours,
    };
}

export function setEvents(events) {
    return {
        type: SET_EVENTS,
        events,
    };
}

export function setEventsFilter(filter) {
    return {
        type: SET_EVENTS_FILTER,
        filter
    };
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_GOOGLE_API_KEY:
            return { ...state, googleApiKey: action.apiKey };
        case SET_GOOGLE_WEB_CLIENT_ID:
            return { ...state, googleWebClientId: action.clientId };
        case ADD_CALENDARS:
            return { ...state, calendars: state.calendars.concat(action.calendars) };
        case SET_CURRENT_CALENDAR:
            return { ...state, currentCalendar: action.id, totalHours: initialState.totalHours };
        case SET_START_DATE:
            return { ...state, startDate: action.date, totalHours: initialState.totalHours };
        case SET_END_DATE:
            return { ...state, endDate: action.date, totalHours: initialState.totalHours };
        case SET_TOTAL_HOURS:
            return { ...state, totalHours: action.hours };
        case SET_EVENTS:
            return { ...state, events: action.events };
        case SET_EVENTS_FILTER:
            return { ...state, eventsFilter: action.filter.toLowerCase() };
        default:
            return { ...state };
    }
};