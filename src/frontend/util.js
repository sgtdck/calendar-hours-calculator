export function calcTotalHours(events) {
    const minutes = events.reduce((result, event) => result + calcEventDurationMinutes(event), 0);
    return Math.floor(minutes / 6) / 10;
}

export function calcEventDurationMinutes(event) {
    const { start, end } = event;
    return (((new Date(end.dateTime)).getTime() - (new Date(start.dateTime)).getTime()) / 1000) / 60;
}
