const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const DefinePlugin = require('webpack').DefinePlugin;
const merge = require('webpack-merge');
const common = require('./webpack.common');
const keys = require('./keys.prod.json');

module.exports = merge(common, {
    mode: "production",
    plugins: [
        new UglifyJSPlugin({
            uglifyOptions: {
                output: {
                    ascii_only: true
                }
            }
        }),
        new DefinePlugin({
            API_KEY: JSON.stringify(keys.apiKey),
            CLIENT_ID: JSON.stringify(keys.clientId)
        })
    ]
});
