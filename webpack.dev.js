const DefinePlugin = require('webpack').DefinePlugin;
const merge = require('webpack-merge');
const common = require('./webpack.common');
const keys = require('./keys.dev.json');

module.exports = merge(common, {
    mode: "development",
    entry: [
        'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server'
    ],
    plugins: [
        new DefinePlugin({
            API_KEY: JSON.stringify(keys.apiKey),
            CLIENT_ID: JSON.stringify(keys.clientId)
        })
    ]
});
