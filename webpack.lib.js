const path = require('path');
const DllPlugin = require('webpack').DllPlugin;

module.exports = {
    context: process.cwd(),
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.less', '.css'],
        modules: [__dirname, 'node_modules']
    },

    entry: {
        library: [
            'date-fns',
            'react',
            'react-day-picker',
            'react-dom',
            'react-redux',
            'react-spinners',
            'redux'
        ]
    },
    output: {
        filename: '[name].dll.js',
        path: path.resolve(__dirname, './dist/lib'),
        library: '[name]'
    },
    plugins: [
        new DllPlugin({
            name: '[name]',
            path: './dist/lib/[name].json'
        })
    ]
};